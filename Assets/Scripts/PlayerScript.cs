﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

    public enum PlayerState
    {
        PLAYERSTATE_ACTIVE,
        PLAYERSTATE_DESTROYED,
        PLAYERSTATE_PREPARING_TO_START,
        PLAYERSTATE_DEAD
    }

    const float MAX_TILT_ANGLE = 40.0f;
    const float MAX_TILT_SPEED = 100.0f;

    const float RESPAWN_TIME = 3.0f;
    const float FLASH_TIME = 1.0f;

    const int BULLET_DAMAGE = 5;
    const int BEAM_DAMAGE = 10;
    const int COLLIDE_DAMAGE = 20;

    const int PLAYER_FULL_HEALTH = 100;

    // player movement
    float playerSpeed = 1.2f;
    Vector3 moveVector;
    Rigidbody playerRigidbody;
    RangeAttribute ZMoveRange = new RangeAttribute(0.27f, 0.85f);
    RangeAttribute XMoveRange = new RangeAttribute(-0.45f, 0.45f);
    float tiltAngle = 0.0f;

    // player shooting
    GameObject bulletBatchGroup;
    Transform bulletSpawnPointLeft;
    Transform bulletSpawnPointRight;
    float playerFireRate = 0.1f;
    float lastFireTime = 9999.0f;
    float projectileSpeed = 2.0f;

    // player state
    public PlayerState currentPlayerState = PlayerState.PLAYERSTATE_PREPARING_TO_START;
    public bool isDead = false;
    public int playerHealth = PLAYER_FULL_HEALTH;
    Vector3 startPoint;
    Quaternion startRotation;
    public AudioClip playerShoot;
    public AudioClip playerHit;
    public AudioClip playerDie;
    public ParticleSystem explosionParticles;

    //
    float timeToRespawn = 0.0f;
    float flashTime = 0.0f;

    Renderer[] renderObjs;

    // Use this for initialization
    void Start () {
        startPoint = transform.position;
        startRotation = transform.rotation;

        currentPlayerState = PlayerState.PLAYERSTATE_PREPARING_TO_START;

        renderObjs = GetComponentsInChildren<Renderer>();

        playerHealth = PLAYER_FULL_HEALTH;
        isDead = false;

        playerRigidbody = GetComponent<Rigidbody>();

        // spawn points for bullets
        bulletSpawnPointLeft = transform.Find("leftTurret");
        bulletSpawnPointRight = transform.Find("rightTurret");

        bulletBatchGroup = GameObject.Find("BulletBatchGroup");
    }
	
    public void Reset()
    {
        if (renderObjs != null)
        {
            for (int i = 0; i < renderObjs.Length; ++i)
                renderObjs[i].enabled = true;
        }

        playerHealth = PLAYER_FULL_HEALTH;

        gameObject.transform.position = startPoint;
        gameObject.transform.rotation = startRotation;

        playerRigidbody.position = startPoint;
        playerRigidbody.rotation = startRotation;

        playerRigidbody.velocity = Vector3.zero;
        playerRigidbody.angularVelocity = Vector3.zero;

        tiltAngle = 0.0f;

        currentPlayerState = PlayerState.PLAYERSTATE_ACTIVE;
        GameSystems.Instance.PlayerRespawned();

        isDead = false;

        timeToRespawn = 0.0f;
        flashTime = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead)
        {
            // prepare to respawn
            if (currentPlayerState == PlayerState.PLAYERSTATE_PREPARING_TO_START)
            {
                flashTime += Time.deltaTime;

                if (flashTime >= FLASH_TIME)
                {
                    // bring the player back to life
                    if (renderObjs != null)
                    {
                        for (int i = 0; i < renderObjs.Length; ++i)
                            renderObjs[i].enabled = true;
                    }

                    flashTime = 0.0f;

                    // ready to start
                    isDead = false;
                    currentPlayerState = PlayerState.PLAYERSTATE_ACTIVE;
                    GameSystems.Instance.PlayerRespawned();
                }
            }
            else
            if (currentPlayerState == PlayerState.PLAYERSTATE_DESTROYED)
            {

                timeToRespawn += Time.deltaTime;
                if (timeToRespawn >= RESPAWN_TIME)
                {
                    // respawn player
                    if (renderObjs != null)
                    {
                        for (int i = 0; i < renderObjs.Length; ++i)
                            renderObjs[i].enabled = true;
                    }

                    // reset
                    playerHealth = PLAYER_FULL_HEALTH;

                    gameObject.transform.position = startPoint;
                    gameObject.transform.rotation = startRotation;

                    playerRigidbody.position = startPoint;
                    playerRigidbody.rotation = startRotation;

                    playerRigidbody.velocity = Vector3.zero;
                    playerRigidbody.angularVelocity = Vector3.zero;

                    tiltAngle = 0.0f;

                    currentPlayerState = PlayerState.PLAYERSTATE_PREPARING_TO_START;

                    flashTime = 0.0f;
                    timeToRespawn = 0.0f;
                }
            }
            else
            if (currentPlayerState == PlayerState.PLAYERSTATE_DEAD)
            {

            }

            return;
        }
        else
        {
            Camera camera = Camera.main;
            Vector3 nearPointA = camera.ScreenToWorldPoint(new Vector3(0, 200, 0.7f));
            Vector3 nearPointB = camera.ScreenToWorldPoint(new Vector3(Screen.width, 200, 0.7f));

            XMoveRange = new RangeAttribute(nearPointA.x, nearPointB.x);

            float h = Input.GetAxisRaw("Horizontal");
            float v = Input.GetAxisRaw("Vertical");

            Move(h, v);

            Tilt(h);

            Shoot();
        }
    }

    void Move(float x, float z)
    {
        // Set the movement vector based on the axis input.
        moveVector.Set(x, 0.0f, z);

        // Normalise the movement vector and make it proportional to the speed per second.
        moveVector = moveVector.normalized * playerSpeed * Time.deltaTime;

        // keep player within a range
        Vector3 newPos = transform.position + moveVector;
        if (newPos.x < XMoveRange.min)
            newPos.x = XMoveRange.min;
        if (newPos.x > XMoveRange.max)
            newPos.x = XMoveRange.max;

        if (newPos.z < ZMoveRange.min)
            newPos.z = ZMoveRange.min;
        if (newPos.z > ZMoveRange.max)
            newPos.z = ZMoveRange.max;

        // Move the player to it's current position plus the movement.
        playerRigidbody.MovePosition(newPos);
    }

    void Tilt(float v)
    {
        // tilt on the z axis with reset as the player goes left/right
        v = Mathf.Clamp(v, -1.0f, 1.0f);

        if( v > 0.1f ) // tilt left
        {
            tiltAngle += MAX_TILT_SPEED * Time.deltaTime;
            if (tiltAngle > MAX_TILT_ANGLE)
                tiltAngle = MAX_TILT_ANGLE;
        }
        else
        if (v < -0.1f) // tilt
        {
            tiltAngle -= MAX_TILT_SPEED * Time.deltaTime;
            if (tiltAngle < -MAX_TILT_ANGLE)
                tiltAngle = -MAX_TILT_ANGLE;
        }
        else
        {
            // reset the angle
            if (tiltAngle > 1.0f)
                tiltAngle -= (MAX_TILT_SPEED * 0.5f) * Time.deltaTime;
            else
            if (tiltAngle < -1.0f)
                tiltAngle += (MAX_TILT_SPEED * 0.5f) * Time.deltaTime;
            else
                tiltAngle = 0.0f;
        }
        Quaternion q = Quaternion.Euler( 0.0f, 180.0f, tiltAngle );

        transform.rotation = q;
    }

    void Shoot()
    {
        // determine if it's time to shoot
        bool firePressed = Input.GetButton("SpaceFire");

        lastFireTime += Time.deltaTime;

        if (firePressed)
        {
            if (lastFireTime >= playerFireRate)
            {
                Vector3 dir = new Vector3(0.0f, 0.0f, 1.0f);

                GameObject leftBulletGameObject = (GameObject)Instantiate(Resources.Load("Projectiles/PlayerBullet"), bulletSpawnPointLeft.position, bulletSpawnPointLeft.rotation);
                BulletScript leftBulletScript = leftBulletGameObject.GetComponent<BulletScript>();

                leftBulletScript.moveDirection = dir;
                leftBulletScript.projectileSpeed = projectileSpeed;

                GameObject rightBulletGameObject = (GameObject)Instantiate(Resources.Load("Projectiles/PlayerBullet"), bulletSpawnPointRight.position, bulletSpawnPointRight.rotation);
                BulletScript rightBulletScript = rightBulletGameObject.GetComponent<BulletScript>();

                rightBulletScript.moveDirection = dir;
                rightBulletScript.projectileSpeed = projectileSpeed;

                leftBulletScript.transform.parent = bulletBatchGroup.transform;
                rightBulletScript.transform.parent = bulletBatchGroup.transform;

                AudioSource.PlayClipAtPoint(playerShoot, Camera.main.transform.position);

                lastFireTime = 0.0f;
            }
        }
    }

    /*void OnCollisionEnter(Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            if (collision.gameObject.tag == "Projectile")
            {
                //print(contact.thisCollider.name + " hit " + contact.otherCollider.name);

                // remove bullet
                Destroy(collision.gameObject);
            }
        }
    }*/

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Projectile")
        {
            if (!isDead)
            {
                // hit by a bullet
                playerHealth -= BULLET_DAMAGE;

                CheckPlayerStateOnHit();

                AudioSource.PlayClipAtPoint(playerHit, Camera.main.transform.position);

                // remove bullet
                Destroy(collider.gameObject);
            }
        }
        else
        if (collider.gameObject.tag == "Enemy")
        {
            EnemyScript enemy = collider.gameObject.GetComponent<EnemyScript>();

            if (!enemy.isDead && !isDead)
            {
                // hit by an enemy
                playerHealth -= COLLIDE_DAMAGE;

                CheckPlayerStateOnHit();

                AudioSource.PlayClipAtPoint(playerHit, Camera.main.transform.position);

                // remove enemy
                Destroy(collider.gameObject);
            }
        }
        else
        if (collider.gameObject.tag == "Boss")
        {
            BossScript boss = collider.gameObject.GetComponent<BossScript>();

            if (!boss.isDead && !isDead)
            {
                // boss collision is instant death
                playerHealth = 0;

                CheckPlayerStateOnHit();
            }
        }
    }

    void CheckPlayerStateOnHit()
    {
        if (playerHealth <= 0) // player has not health
        {
            ParticleSystem particleSystem = (ParticleSystem)Instantiate(explosionParticles, transform.position, transform.rotation);
            Destroy(particleSystem.gameObject, particleSystem.duration);

            // disable rendering
            if (renderObjs != null)
            {
                for (int i = 0; i < renderObjs.Length; ++i)
                    renderObjs[i].enabled = false;
            }

            // decrease life
            GameSystems.Instance.DecreasePlayerLives();

            // all live used up
            if (GameSystems.Instance.lives <= 0)
            {
                currentPlayerState = PlayerState.PLAYERSTATE_DEAD;

                GameSystems.Instance.ChangeUIState(GameSystems.MenuState.MENUSTATE_GAMEOVER);
            }
            else
            {
                // can respawn
                currentPlayerState = PlayerState.PLAYERSTATE_DESTROYED;
                timeToRespawn = 0.0f;
            }

            AudioSource.PlayClipAtPoint(playerDie, Camera.main.transform.position);

            GameSystems.Instance.PlayerDied((GameSystems.Instance.lives==0));
            isDead = true;
        }
    }
}