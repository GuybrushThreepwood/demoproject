﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

public class GameSystems : MonoBehaviour
{
    // singleton access
    private static GameSystems m_Instance;
    public static GameSystems Instance { get { return m_Instance; } }

    // menu states
    public enum MenuState
    {
        MENUSTATE_MAIN,
        MENUSTATE_GAME,
        MENUSTATE_GAMEOVER,
        MENUSTATE_GAMEOVER_WAIT
    }

    const float FLASH_TIME = 1.0f;
    const int START_LIVES = 3;

    // UI values
    int playerScore { get; set; }
    int playerHiscore { get; set; }
    int multiplier { get; set; }
    public int lives { get; set; }

    // UI elements
    public Text playerScoreText;
    public Text hiscoreText;
    public Text playerLives;
    public Text scoreMultiplierText;

    public Text mainTitle;
    public Text pressFire;
    public Text musicTitle;
    public Text pauseTitle;
    public Text returnToMain;
    public RawImage fader;

    // systems and objects
    public EnemyGenerator enemyGenerator;
    LevelGenAndMove levelGenAndMove;
    GameObject playerShip;
    PlayerScript playerScript;
    public GameObject bossShip;
    BossScript bossScript;
    GameObject cloudGenerator;
    GameObject enemyGroup;

    // audio
    public AudioClip multiplierUp;
    public AudioClip gameMusicClip;
    public AudioClip bossMusicClip;
    AudioSource musicAudioSource;
    MusicFader musicScript;
    bool musicToggle = true;

    // state
    public MenuState menuState;

    // timing during a pause
    float lastTime;
    bool isPaused = false;

    // text colour cycle
    Color colour = Color.red;
    HSBColor hsbc;
    float cycleTime = 5.0f;

    // death/return to menu
    bool cameraTransition = false;
    bool flashToggle = true;
    float flashTime = 0.0f;
    ScreenFader screenFaderScript;

    // game over
    bool gameOverFade = false;

    // boss 
    int totalLevelPasses = 0;
    bool bossHasAppeared = false;
    bool bossIsDead = false;

    // track enemies destroyed and alive in a batch
    List<KeyValuePair<int, int>> batchDestroyedList = new List<KeyValuePair<int, int>>();

    void Awake()
    {
        m_Instance = this;
        menuState = MenuState.MENUSTATE_MAIN;

        Load();
    }

    void OnDestroy()
    {
        m_Instance = null;
    }

    // Use this for initialization
    void Start()
    {
        lives = START_LIVES;
        multiplier = 1;

        musicAudioSource = GetComponent<AudioSource>();
        musicScript = GetComponent<MusicFader>();

        // start of colour cycle
        hsbc = HSBColor.FromColor(colour);

        if (menuState == MenuState.MENUSTATE_MAIN)
        {
            SetMainMenuState();
        }
        else
        if (menuState == MenuState.MENUSTATE_GAME)
        {
            SetGameState();
        }

        cloudGenerator = GameObject.Find("CloudSpawn");
        enemyGroup = GameObject.Find("EnemyGroup");

        playerScoreText.text = string.Format("{0}", playerScore);
        hiscoreText.text = string.Format("{0}", playerHiscore);
        playerLives.text = string.Format("x{0}", lives);
        scoreMultiplierText.text = string.Format("(x{0})", multiplier);

        screenFaderScript = fader.GetComponent<ScreenFader>();
        screenFaderScript.Hide();

        enemyGenerator = GetComponent<EnemyGenerator>();
        levelGenAndMove = GetComponent<LevelGenAndMove>();

        playerShip = GameObject.Find("PlayerShip");
        playerScript = playerShip.GetComponent<PlayerScript>();
        playerShip.SetActive(false);

        enemyGenerator.SetGenerationState(true, true);
        levelGenAndMove.SetGenerationState(true, false);
        cloudGenerator.SetActive(false);

        bossHasAppeared = false;

        lastTime = Time.realtimeSinceStartup;
    }

    // Update is called once per frame
    void Update()
    {
        float currentTime = Time.realtimeSinceStartup;

        float delta = currentTime - lastTime;
        lastTime = currentTime;

        bool musicState = Input.GetButtonDown("Music");
        if (musicState)
        {
            musicToggle = !musicToggle;

            if (musicToggle)
                musicAudioSource.mute = false;
            else
                musicAudioSource.mute = true;
        }

        if (cameraTransition)
        {
            // start the main game
            playerShip.SetActive(true);

            enemyGenerator.SetGenerationState(false, false);
            levelGenAndMove.SetGenerationState(false, false);
            cloudGenerator.SetActive(true);

            // start the game
            menuState = MenuState.MENUSTATE_GAME;
            cameraTransition = false;
        }
        else
        {
            if (menuState == MenuState.MENUSTATE_MAIN)
            {
                bool quitGame = Input.GetButtonDown("Quit");
                if (quitGame)
                    Application.Quit();

                bool firePressed = Input.GetButtonUp("SpaceFire");

                CycleTitleText(delta);
                ProcessTitleScreen();

                // transition to main game
                if (firePressed)
                {
                    SetGameState();

                    cameraTransition = true;
                }
            }
            else
            if (menuState == MenuState.MENUSTATE_GAME)
            {
                bool pausePressed = Input.GetButtonDown("Pause");

                if (isPaused)
                    CycleTitleText(delta);

                // pause state change
                if (pausePressed)
                {
                    isPaused = !isPaused;
                    if (isPaused)
                    {
                        SetPaused();
                        Time.timeScale = 0.0f;
                    }
                    else
                    {
                        SetGameState();
                        Time.timeScale = 1.0f;
                    }
                }

                bool returnButton = (Input.GetButtonDown("ReturnToMain") || Input.GetButtonDown("Quit"));
                if( returnButton )
                {
                    playerShip.SetActive(false);
                    playerScript.Reset();

                    levelGenAndMove.SetGenerationState(true, false);
                    levelGenAndMove.Reset();

                    enemyGenerator.SetGenerationState(true, true);
                    enemyGenerator.DestroyAllEnemies();
                    cloudGenerator.SetActive(false);

                    ResetLevel();
                    Save();
                }
            }
            else
            if (menuState == MenuState.MENUSTATE_GAMEOVER)
            {
                if (gameOverFade)
                {
                    // if fade complete
                    if (screenFaderScript.FinishedFade())
                    {
                        levelGenAndMove.SetGenerationState(true, false);
                        levelGenAndMove.Reset();

                        enemyGenerator.SetGenerationState(true, true);
                        enemyGenerator.DestroyAllEnemies();
                        cloudGenerator.SetActive(false);

                        gameOverFade = false;

                        playerScript.Reset();

                        SetGameOver();

                        // we are back in the main menu
                        menuState = MenuState.MENUSTATE_GAMEOVER_WAIT;
                    }
                }
            }
            else
            if (menuState == MenuState.MENUSTATE_GAMEOVER_WAIT)
            {
                // now reset the level
                bool firePressed = Input.GetButtonUp("SpaceFire");
                if (firePressed)
                {
                    ResetLevel();
                    Save();
                }
            }
        }
    }

    void CycleTitleText( float deltaTime )
    {
        hsbc.h = (hsbc.h + deltaTime / cycleTime) % 1.0f;
        mainTitle.color = HSBColor.ToColor(hsbc);
    }

    void ProcessTitleScreen()
    {
        // flash the press fire title
        flashTime += Time.deltaTime;
        if (flashTime >= FLASH_TIME)
        {
            flashToggle = !flashToggle;
            pressFire.enabled = flashToggle;

            flashTime = 0.0f;
        }
    }

    void SetMainMenuState()
    {
        playerScoreText.enabled = false;
        hiscoreText.enabled = false;
        playerLives.enabled = false;
        scoreMultiplierText.enabled = false;

        mainTitle.enabled = true;
        mainTitle.text = "demo project";
        pressFire.enabled = true;

        musicTitle.enabled = true;
        pauseTitle.enabled = true;
        returnToMain.enabled = true;
    }

    void SetGameState()
    {
        playerScoreText.enabled = true;
        hiscoreText.enabled = true;
        playerLives.enabled = true;
        scoreMultiplierText.enabled = true;

        mainTitle.enabled = false;
        mainTitle.text = "demo project";
        pressFire.enabled = false;

        musicTitle.enabled = false;
        pauseTitle.enabled = false;
        returnToMain.enabled = false;
    }

    void SetPaused()
    {
        playerScoreText.enabled = true;
        hiscoreText.enabled = true;
        playerLives.enabled = true;
        scoreMultiplierText.enabled = true;

        mainTitle.enabled = true;
        mainTitle.text = "paused";
        pressFire.enabled = false;

        musicTitle.enabled = false;
        pauseTitle.enabled = false;
        returnToMain.enabled = false;
    }

    void SetGameOver()
    {
        playerScoreText.enabled = true;
        hiscoreText.enabled = true;
        playerLives.enabled = true;
        scoreMultiplierText.enabled = true;

        mainTitle.enabled = true;
        mainTitle.text = "game over";
        mainTitle.color = Color.white;

        pressFire.enabled = false;

        musicTitle.enabled = false;
        pauseTitle.enabled = false;
        returnToMain.enabled = false;
    }

    void ResetLevel()
    {
        // reset the level
        SetMainMenuState();

        screenFaderScript.Hide();

        playerScore = 0;
        lives = START_LIVES;
        totalLevelPasses = 0;

        playerScoreText.text = string.Format("{0}", playerScore);
        playerLives.text = string.Format("x{0}", lives);

        ResetMultiplier();

        levelGenAndMove.Reset();

        bossHasAppeared = false;
        bossIsDead = false;

        // we are back in the main menu
        menuState = MenuState.MENUSTATE_MAIN;
    }

    public void AddToPlayerScore(int val)
    {
        playerScore += (val * multiplier);

        playerScoreText.text = string.Format("{0}", playerScore);

        if (playerScore > playerHiscore)
        {
            playerHiscore = playerScore;
            hiscoreText.text = string.Format("{0}", playerScore);
        }
    }

    public void IncreaseMultiplier(int val)
    {
        multiplier += val;

        scoreMultiplierText.text = string.Format("(x{0})", multiplier);
    }

    public void ResetMultiplier()
    {
        multiplier = 1;

        scoreMultiplierText.text = string.Format("(x{0})", multiplier);
    }

    public void AddEnemyToDestroyed(int batchId, int batchIndex)
    {
        KeyValuePair<int, int> pair = new KeyValuePair<int, int>(batchId, batchIndex);

        batchDestroyedList.Add(pair);

        int totalDestroyed = 0;
        foreach (KeyValuePair<int, int> item in batchDestroyedList)
        {
            if (item.Key == batchId)
            {
                totalDestroyed++;
            }
        }

        if (totalDestroyed == 5)
        {
            IncreaseMultiplier(1);

            AudioSource.PlayClipAtPoint(multiplierUp, Camera.main.transform.position);
        }
    }

    public void EnemyBatchComplete(int batchId)
    {
        int totalDestroyed = 0;

        for (int i = batchDestroyedList.Count - 1; i >= 0; i--)
        {
            if (batchDestroyedList[i].Key == batchId)
            {
                totalDestroyed++;
            }
            batchDestroyedList.RemoveAt(i);
        }

        if (totalDestroyed != 5)
            ResetMultiplier();
    }

    public void DecreasePlayerLives()
    {
        lives--;

        if (lives < 0)
            lives = 0;

        playerLives.text = string.Format("x{0}", lives);
    }

    public void IncreaseLevelPasses()
    {
        if (menuState == MenuState.MENUSTATE_GAMEOVER)
            return;

        if (!bossHasAppeared)
        {
            totalLevelPasses++;

            if (totalLevelPasses == 1)
            {
                // boss time
                enemyGenerator.SetGenerationState(true, true);
                musicScript.FadeMusic( bossMusicClip );
                bossHasAppeared = true;
                bossIsDead = false;

                GameObject spawnedBoss = (GameObject)Instantiate(bossShip);
                spawnedBoss.transform.parent = enemyGroup.transform;

                bossScript = spawnedBoss.GetComponent<BossScript>();
            }
        }
    }

    public void ChangeUIState(MenuState state)
    {
        menuState = state;

        if (menuState == MenuState.MENUSTATE_GAMEOVER)
        {
            gameOverFade = true;

            playerShip.SetActive(false);

            if( bossHasAppeared &&
                !bossIsDead )
            {
                musicScript.FadeMusic(gameMusicClip);
                Destroy(bossShip);
            }

            enemyGenerator.SetGenerationState(true, true);
            screenFaderScript.FadeToBlack();
        }
    }

    public void BossDeath()
    {
        // check any boss
        if (bossHasAppeared &&
            bossIsDead == false)
        {
           // start generation again
           enemyGenerator.SetGenerationState(false, true);
           musicScript.FadeMusic(gameMusicClip);

           bossIsDead = true;
        }
    }

    public void PlayerRespawned()
    {
        if (menuState != MenuState.MENUSTATE_GAME)
            return;

        if (bossHasAppeared)
        {
            if (bossIsDead)
                GameSystems.Instance.enemyGenerator.SetGenerationState(false, true);
            else
                bossScript.pauseShooting = false;
        }
        else
            GameSystems.Instance.enemyGenerator.SetGenerationState(false, true);
    }

    public void PlayerDied(bool noLives)
    {
        if( bossHasAppeared &&
            !bossIsDead )
        {
            bossScript.pauseShooting = true;
        }

        GameSystems.Instance.enemyGenerator.SetGenerationState(true, true);
    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();

        FileStream file = File.Open(Application.persistentDataPath + "/score.data", FileMode.OpenOrCreate);

        GameData data = new GameData();
        data.hiscore = playerHiscore;

        bf.Serialize(file, data);
        file.Close();
    }

    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/score.data"))
        {
            BinaryFormatter bf = new BinaryFormatter();

            FileStream file = File.Open(Application.persistentDataPath + "/score.data", FileMode.Open);

            GameData data = (GameData)bf.Deserialize(file);
            file.Close();

            playerHiscore = data.hiscore;
        }
    }

}

[System.Serializable]
public class GameData
{
    public int hiscore;
}

