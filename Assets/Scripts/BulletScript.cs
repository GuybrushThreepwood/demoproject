﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {

    const float MIN_Z_OUT = 0.0f;
    const float MAX_Z_OUT = 2.0f;

    public float projectileSpeed = 0.75f;
    public Vector3 moveDirection = new Vector3(0.0f,0.0f,1.0f);

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        // keep moving the bullets
        transform.position += (moveDirection * projectileSpeed) * Time.deltaTime;

        // moved far enough to destroy
        if ((transform.position.z >= MAX_Z_OUT))
        {
            Destroy(this.gameObject);
        }
        else
        if ((transform.position.z <= MIN_Z_OUT))
        {
            Destroy(this.gameObject);
        }	
	}
}
