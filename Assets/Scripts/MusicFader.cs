﻿using UnityEngine;
using System.Collections;

public class MusicFader : MonoBehaviour {

    const float VOLUME_SCALE_SPEED = 0.1f;

    enum Fade
    {
        FADE_NONE,
        FADE_IN,
        FADE_OUT
    }

    bool processFade = false;

    float currentVolume;
    float srcVolume;
    Fade fadeState;

    public AudioSource audioSource;
    AudioClip currentClip;
    AudioClip destinationClip;

    // Use this for initialization
    void Start () {
        currentClip = audioSource.clip;
        srcVolume = audioSource.volume;
        currentVolume = srcVolume;

        processFade = false;
        fadeState = Fade.FADE_NONE;
    }
	
	// Update is called once per frame
	void Update () {
	    if( processFade )
        {
            if( fadeState == Fade.FADE_OUT )
            {
                currentVolume -= VOLUME_SCALE_SPEED * Time.deltaTime;
                if( currentVolume <= 0 )
                {
                    currentVolume = 0;

                    audioSource.Stop();
                    audioSource.clip = destinationClip;
                    audioSource.Play();

                    fadeState = Fade.FADE_IN;
                }

                audioSource.volume = currentVolume;
            }
            else
            if (fadeState == Fade.FADE_IN)
            {
                currentVolume += VOLUME_SCALE_SPEED * Time.deltaTime;
                if (currentVolume >= srcVolume)
                {
                    currentVolume = srcVolume;

                    currentClip = destinationClip;

                    fadeState = Fade.FADE_NONE;
                    processFade = false;
                }

                audioSource.volume = currentVolume;
            }
        }
	}

    public void FadeMusic( AudioClip destClip )
    {
        if (!processFade)
        {
            if (currentClip != destClip)
            {
                fadeState = Fade.FADE_OUT;
                processFade = true;

                destinationClip = destClip;

                srcVolume = audioSource.volume;
                currentVolume = srcVolume;
            }
        }
    }
}
