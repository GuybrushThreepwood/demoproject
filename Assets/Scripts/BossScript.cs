﻿using UnityEngine;
using System.Collections;

public class BossScript : MonoBehaviour {

    enum BossState
    {
        BOSS_STATE_ENTERING,
        BOSS_STATE_NORMAL,
        BOSS_STATE_DYING,
        BOSS_STATE_DEAD
    }

    const int SCORE_WORTH = 5000;

    const float BEAM_TIME_MIN = 10.0f;
    const float BEAM_TIME_MAX = 15.0f;

    const float SHOOT_TIME_MIN = 2.0f;
    const float SHOOT_TIME_MAX = 5.0f;
    const float SHOOT_TIME_RAPID = 0.5f;

    const float SHOOT_SPEED_MIN = 0.5f;
    const float SHOOT_SPEED_MAX = 0.75f;

    const float MOVE_SPEED = 1.0f;
    const float EPSILON = 0.1f;

    const int BEAM_SHOT_COUNT = 6;
    const float BEAM_SPACING = 0.5f;
    const float BEAM_SPEED = 1.5f;

    const int BOSS_HEALTH = 1000;
    const float DEATH_TIME = 5.0f;

    // object info
    Transform bulletSpawnPointLeft;
    Transform bulletSpawnPointRight;
    GameObject playerShip;

    Vector3 startPoint;

    // shoot info
    float timeToShoot = 0.0f;
    float waitShootTime = SHOOT_TIME_MIN;

    float timeToBeam = 0.0f;
    float waitBeamTime = BEAM_TIME_MIN;

    bool rapidFire = false;
    int rapidFireCount = 0;

    bool fireBeam = false;
    bool beamsDestroyed = false;
    bool leftTurret = true;
    int totalBeams = 0;

    // movement
    bool moveToPoint = false;
    float timeToMove = 0.0f;
    Vector3 moveDirection;
    Vector3 randomMovePoint;
    Vector3 enterPoint;
    Vector3 enemyToPlayer;

    // stats
    public bool pauseShooting = false;
    GameObject bulletBatchGroup;
    public int currentHealth = BOSS_HEALTH;
    public bool isDead = false;
    BossState currentState = BossState.BOSS_STATE_ENTERING;
    public ParticleSystem explosionParticles;
    float deathTime;
    float deathExplosionTime;

    // audio
    public AudioClip bossShootBeam;
    public AudioClip bossShootBullets;
    public AudioClip bossDeath;
    public AudioClip bossHit;

    // Use this for initialization
    void Start () {
        bulletSpawnPointLeft = transform.Find("leftTurret");
        bulletSpawnPointRight = transform.Find("rightTurret");

        bulletBatchGroup = GameObject.Find("BulletBatchGroup");

        currentHealth = BOSS_HEALTH;
        currentState = BossState.BOSS_STATE_ENTERING;
        isDead = false;

        startPoint = transform.position;
        //startRotation = transform.rotation;

        playerShip = GameObject.Find("PlayerShip");

        // enter point
        enterPoint = startPoint;
        enterPoint.z = 1.2f;
        moveDirection = (enterPoint - transform.position).normalized;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentState == BossState.BOSS_STATE_NORMAL)
        {
            ProcessMovement();

            if (!pauseShooting)
            {
                ProcessShots();

                // less than half health means the beam is dead
                if (currentHealth > BOSS_HEALTH / 2)
                    ProcessBeam();
            }
        }
        else
        if (currentState == BossState.BOSS_STATE_ENTERING)
        {
            // keep moving until we are close enough to the point
            transform.position += (moveDirection * (MOVE_SPEED * 0.25f)) * Time.deltaTime;

            Vector3 diff = transform.position - enterPoint;

            if (Mathf.Abs(diff.x) <= EPSILON &&
                Mathf.Abs(diff.z) <= EPSILON)
            {
                currentState = BossState.BOSS_STATE_NORMAL;
            }
        }
        else
        if (currentState == BossState.BOSS_STATE_DYING)
        {
            // handle death
            deathTime += Time.deltaTime;
            if (deathTime >= DEATH_TIME)
            {
                currentState = BossState.BOSS_STATE_DEAD;
                GameSystems.Instance.BossDeath();

                ParticleSystem particleSystem = (ParticleSystem)Instantiate(explosionParticles, transform.position, transform.rotation);
                particleSystem.gameObject.transform.localScale = new Vector3(5.0f, 5.0f, 5.0f);
                Destroy(particleSystem.gameObject, particleSystem.duration);

                Destroy(this.gameObject);
            }
            else
            {
                // explosions from random offsets of the origin
                deathExplosionTime += Time.deltaTime;
                if (deathExplosionTime > 0.5f )
                {
                    Vector3 randomOffset = new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f));

                    ParticleSystem particleSystem = (ParticleSystem)Instantiate(explosionParticles, transform.position+randomOffset, transform.rotation);
                    Destroy(particleSystem.gameObject, particleSystem.duration);

                    AudioSource.PlayClipAtPoint(bossDeath, Camera.main.transform.position);
                    deathExplosionTime = 0.0f;
                }
            }
        }
    }

    void ProcessMovement()
    {
        // Create a vector from the player to this
        enemyToPlayer = transform.position - playerShip.transform.position;
        enemyToPlayer.y = 0.0f;

        // Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
        Quaternion newRotation = Quaternion.LookRotation(enemyToPlayer);

        transform.rotation = Quaternion.Slerp( transform.rotation, newRotation, 0.5f );

        if (moveToPoint)
        {
            // keep moving until we are close enough to the random point
            transform.position += (moveDirection * MOVE_SPEED) * Time.deltaTime;

            Vector3 diff = transform.position - randomMovePoint;

            if (Mathf.Abs(diff.x) <= EPSILON &&
                Mathf.Abs(diff.z) <= EPSILON)
            {
                moveToPoint = false;
            }
        }
        else
        {
            // decide via time when to move again
            timeToMove += Time.deltaTime;
            if (timeToMove >= 5.0f)
            {
                randomMovePoint.x = Random.Range(-0.5f, 0.5f);
                randomMovePoint.y = transform.position.y;
                randomMovePoint.z = Random.Range(0.75f, 1.4f);

                moveDirection = (randomMovePoint - transform.position).normalized;

                moveToPoint = true;
                timeToMove = 0.0f;
            }
        }
    }

    void ProcessShots()
    {
        if (!fireBeam)
        {
            // shoot spread bullets when not shooting the beam
            timeToShoot += Time.deltaTime;

            if (timeToShoot >= waitShootTime)
            {
                int i = 0;
                Vector3 rotVector;

                // spawn three bullets 
                for (i = 0; i < 3; ++i)
                {
                    GameObject leftGameObject = (GameObject)Instantiate(Resources.Load("Projectiles/EnemyBullet"), bulletSpawnPointLeft.position, bulletSpawnPointLeft.rotation);
                    BulletScript leftBulletScript = leftGameObject.GetComponent<BulletScript>();

                    GameObject rightGameObject = (GameObject)Instantiate(Resources.Load("Projectiles/EnemyBullet"), bulletSpawnPointRight.position, bulletSpawnPointRight.rotation);
                    BulletScript rightBulletScript = rightGameObject.GetComponent<BulletScript>();

                    leftGameObject.transform.parent = bulletBatchGroup.transform;
                    rightGameObject.transform.parent = bulletBatchGroup.transform;

                    if (i == 0)
                    {
                        rightBulletScript.projectileSpeed = leftBulletScript.projectileSpeed = Random.Range( SHOOT_SPEED_MIN, SHOOT_SPEED_MAX);

                        rotVector = Quaternion.AngleAxis(-10, Vector3.up) * -bulletSpawnPointRight.forward.normalized;
                        rightBulletScript.moveDirection = rotVector.normalized;

                        rotVector = Quaternion.AngleAxis(-10, Vector3.up) * -bulletSpawnPointLeft.forward.normalized;
                        leftBulletScript.moveDirection = rotVector.normalized;
                    }
                    else
                    if (i == 1)
                    {
                        rightBulletScript.projectileSpeed = leftBulletScript.projectileSpeed = Random.Range(SHOOT_SPEED_MIN, SHOOT_SPEED_MAX);

                        rightBulletScript.moveDirection = -bulletSpawnPointRight.forward.normalized;
                        leftBulletScript.moveDirection = -bulletSpawnPointLeft.forward.normalized;
                    }
                    else
                    if (i == 2)
                    {
                        rightBulletScript.projectileSpeed = leftBulletScript.projectileSpeed = Random.Range(SHOOT_SPEED_MIN, SHOOT_SPEED_MAX);

                        rotVector = Quaternion.AngleAxis(10, Vector3.up) * -bulletSpawnPointRight.forward.normalized;
                        rightBulletScript.moveDirection = rotVector.normalized;

                        rotVector = Quaternion.AngleAxis(10, Vector3.up) * -bulletSpawnPointLeft.forward.normalized;
                        leftBulletScript.moveDirection = rotVector.normalized;
                    }
                }

                AudioSource.PlayClipAtPoint(bossShootBullets, Camera.main.transform.position);

                if (rapidFire)
                {
                    rapidFireCount++;
                    if (rapidFireCount < 5)
                        waitShootTime = SHOOT_TIME_RAPID;
                    else
                    {
                        rapidFireCount = 0;
                        waitShootTime = SHOOT_TIME_MIN;
                    }
                }

                timeToShoot = 0.0f;
            }
        }
    }

    void ProcessBeam()
    {
        timeToBeam += Time.deltaTime;

        // fire alternating beams from the turrets
        if (timeToBeam >= waitBeamTime)
        {
            if (totalBeams < BEAM_SHOT_COUNT)
            {
                fireBeam = true;

                Vector3 dir;
                GameObject bulletGameObject;
                Vector3 angles = transform.rotation.eulerAngles;

                if (leftTurret)
                {
                    dir = -bulletSpawnPointLeft.forward.normalized;
                    bulletGameObject = (GameObject)Instantiate(Resources.Load("Projectiles/BossBeam"));
                    bulletGameObject.transform.position = bulletSpawnPointLeft.position;
                    bulletGameObject.transform.rotation *= Quaternion.AngleAxis(-angles.y, Vector3.forward.normalized);

                    bulletGameObject.transform.parent = bulletBatchGroup.transform;
                }
                else
                {
                    dir = -bulletSpawnPointRight.forward.normalized;
                    bulletGameObject = (GameObject)Instantiate(Resources.Load("Projectiles/BossBeam"));
                    bulletGameObject.transform.position = bulletSpawnPointRight.position;
                    bulletGameObject.transform.rotation *= Quaternion.AngleAxis(-angles.y, Vector3.forward.normalized);

                    bulletGameObject.transform.parent = bulletBatchGroup.transform;
                }

                leftTurret = !leftTurret;

                BulletScript bulletScript = bulletGameObject.GetComponent<BulletScript>();

                bulletScript.moveDirection = dir;
                bulletScript.projectileSpeed = BEAM_SPEED;

                waitBeamTime = BEAM_SPACING;
                timeToBeam = 0.0f;

                AudioSource.PlayClipAtPoint(bossShootBeam, Camera.main.transform.position);

                totalBeams++;
            }
            else
            {
                fireBeam = false;

                timeToBeam = 0.0f;
                totalBeams = 0;
                waitBeamTime = Random.Range(SHOOT_TIME_MIN, SHOOT_TIME_MAX);
            }
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        // collision between player and player bullets
        if (collider.gameObject.tag == "Projectile" &&
            collider.gameObject.layer == LayerMask.NameToLayer("PlayerProjectile"))
        {
            if (currentState == BossState.BOSS_STATE_NORMAL)
            {
                if (!isDead)
                {
                    // is the boss dead
                    currentHealth -= 1;
                    if (currentHealth <= 0)
                    {
                        isDead = true;

                        currentState = BossState.BOSS_STATE_DYING;

                        GameSystems.Instance.AddToPlayerScore(SCORE_WORTH);

                        deathTime = 0.0f;
                    }

                    // health is less then half with disable the beams and decrease the shoot timer
                    if (currentHealth < BOSS_HEALTH / 2)
                    {
                        if (!beamsDestroyed)
                        {
                            ParticleSystem particleSystem = (ParticleSystem)Instantiate(explosionParticles, bulletSpawnPointLeft.position, bulletSpawnPointLeft.rotation);
                            Destroy(particleSystem.gameObject, particleSystem.duration);

                            particleSystem = (ParticleSystem)Instantiate(explosionParticles, bulletSpawnPointRight.position, bulletSpawnPointRight.rotation);
                            Destroy(particleSystem.gameObject, particleSystem.duration);

                            AudioSource.PlayClipAtPoint(bossDeath, Camera.main.transform.position);

                            fireBeam = false;
                            beamsDestroyed = true;

                            rapidFire = true;
                            rapidFireCount = 0;
                            waitShootTime = SHOOT_TIME_RAPID;
                        }
                    }
                }
            }
            
            // remove bullet
            Destroy(collider.gameObject);
        }
    }
}
