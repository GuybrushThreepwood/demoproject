﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScreenFader : MonoBehaviour {

    const float ALPHA_SPEED = 0.25f;

    RectTransform rect;
    RawImage rawImage;

    bool runFade = false;
    float currentAlpha = 0.0f;
    float destinationAlpha = 0.0f;

    // Use this for initialization
    void Start () {
        rect = GetComponent<RectTransform>();
        rawImage = GetComponent<RawImage>();

        rawImage.color = new Color(0.0f, 0.0f, 0.0f, 0.0f);

        rect.sizeDelta = new Vector2(Screen.width, Screen.height);
    }
	
	// Update is called once per frame
	void Update () {
	    if(runFade)
        {
            currentAlpha += ALPHA_SPEED * Time.deltaTime;
            if(currentAlpha >= destinationAlpha)
            {
                currentAlpha = destinationAlpha;
                runFade = false;
            }

            rawImage.color = new Color(rawImage.color.r, rawImage.color.g, rawImage.color.b, currentAlpha);
        }
	}

    public void Hide()
    {
        currentAlpha = 0.0f;
        rawImage.color = new Color(rawImage.color.r, rawImage.color.g, rawImage.color.b, currentAlpha);
    }

    public void FadeToBlack()
    {
        runFade = true;
        destinationAlpha = 1.0f;
    }

    public bool FinishedFade()
    {
        return !runFade;
    }
}
