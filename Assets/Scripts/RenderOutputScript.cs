﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RenderOutputScript : MonoBehaviour {

    RectTransform rect;
    RawImage rawImage;

    public Camera cameraOut;

    // Use this for initialization
    void Start()
    {
        rawImage = GetComponent<RawImage>();
        rect = GetComponent<RectTransform>();

        // resize
        rect.sizeDelta = new Vector2(Screen.width, Screen.height);

        rawImage.texture = cameraOut.targetTexture;
    }

    // Update is called once per frame
    void Update()
    {
        // make sure the UI is the correct size and assigned the correct target texture
        if(rect.sizeDelta.x != Screen.width ||
            rect.sizeDelta.y != Screen.height )
        {
            rect.sizeDelta = new Vector2(Screen.width, Screen.height);
            rawImage.texture = cameraOut.targetTexture;
        }
    }
}
