﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {

    enum AxisMove
    {
        AXIS_MOVE_X,
        AXIS_MOVE_Z,
    }

    const float SHOOT_TIME_MIN = 2.0f;
    const float SHOOT_TIME_MAX = 5.0f;

    const float SHOOT_SPEED_MIN = 0.5f;
    const float SHOOT_SPEED_MAX = 0.75f;
    const float EPSILON = 0.1f;

    const int SCORE_WORTH = 100;

    const int LAST_ENEMY_ID = 4; // if batch is 5

    // data 
    public int batchId;
    public int batchIndex;

    // pattern info
    public Enemy.Patterns pattern = Enemy.Patterns.PATTERN_STRAIGHT;
    public Vector3 midPoint;
    public Vector3 endPoint;
    public bool spawnedAtTop;
    public bool needsMidPointChange;
    public GameObject bulletBatchGroup;
    public AudioClip enemyShoot;
    public AudioClip enemyDeath;
    public ParticleSystem particlesDeath;

    // player
    GameObject playerShip;
    PlayerScript playerScript;

    // movement
    float movementSpeed = 0.5f;
    AxisMove moveAxisFirst;
    bool swapDirection;
    Vector3 direction;
    Vector3 pos;

    // shooting
    Renderer[] renderObjs;
    float timeToShoot = 0.0f;
    float waitShootTime = SHOOT_TIME_MIN;
    bool canShoot = true;
    public bool isDead = false;

    // Use this for initialization
    void Start () {

        pos = new Vector3(transform.position.x, transform.position.y, transform.position.z);

        playerShip = GameObject.Find("PlayerShip");
        playerScript = playerShip.GetComponent<PlayerScript>();
        
        renderObjs = GetComponentsInChildren<Renderer>();

        switch (pattern)
        {
            case Enemy.Patterns.PATTERN_STRAIGHT:
                {
                    if (spawnedAtTop)
                    {
                        // direction is z only
                        endPoint = transform.position;
                        endPoint.z = -pos.z * 2.0f;

                        direction = new Vector3(0.0f, 0.0f, -1.0f);
                    }
                    else
                    {
                        // direction is x only
                        endPoint = transform.position;
                        endPoint.x = -pos.x * 2.0f;
                        direction = endPoint - pos;
                    }
                }
                break;
            case Enemy.Patterns.PATTERN_SQUARE:
                {
                    // move along an axis and then swap
                    if (spawnedAtTop)
                        moveAxisFirst = AxisMove.AXIS_MOVE_Z;
                    else
                        moveAxisFirst = AxisMove.AXIS_MOVE_X;

                    if( needsMidPointChange ) // corner to corner
                    {
                        Vector3 diff = endPoint - transform.position;
                        midPoint = pos + (diff * 0.5f);
                        moveAxisFirst = AxisMove.AXIS_MOVE_X; 
                    }

                    swapDirection = false;

                    if ( moveAxisFirst == AxisMove.AXIS_MOVE_X )
                    {
                        if ( endPoint.x < pos.x)
                        {
                            direction = new Vector3(-1.0f, 0.0f, 0.0f);
                        }
                        else
                        {
                            direction = new Vector3(1.0f, 0.0f, 0.0f);
                        }
                    }
                    else
                    if (moveAxisFirst == AxisMove.AXIS_MOVE_Z )
                    {
                        if (endPoint.z < pos.z)
                        {
                            direction = new Vector3(0.0f, 0.0f, -1.0f);
                        }
                        else
                        {
                            direction = new Vector3(0.0f, 0.0f, 1.0f);
                        }
                    }
                }
                break;
            /*case Enemy.Patterns.PATTERN_CURVE:
                {

                }
                break;
            case Enemy.Patterns.PATTERN_WAVE:
                {

                }
                break;*/
        }

        isDead = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsTargetVisible(Camera.main, this.gameObject))
            canShoot = true;
        else
            canShoot = false;

        ProcessPatternMovement();

        if (playerScript)
        {
            // do not shoot if the player is dead or respawning
            if (playerScript.currentPlayerState != PlayerScript.PlayerState.PLAYERSTATE_DEAD &&
               playerScript.currentPlayerState != PlayerScript.PlayerState.PLAYERSTATE_DESTROYED)
            {
                ProcessShots();
            }
        }
    }

    void ProcessPatternMovement()
    {
        switch( pattern )
        {
            case Enemy.Patterns.PATTERN_STRAIGHT:
                {
                    transform.position += (direction.normalized * movementSpeed) * Time.deltaTime;

                    Vector3 diff = transform.position - endPoint;

                    if (spawnedAtTop)
                    {
                        if (Mathf.Abs(diff.z) <= EPSILON)
                        {
                            if (batchIndex == LAST_ENEMY_ID) // all enemies in batch have arrived
                            {
                                GameSystems.Instance.EnemyBatchComplete(batchId);
                            }

                            Destroy(this.gameObject);
                        }
                    }
                    else
                    {
                        if (Mathf.Abs(diff.x) <= EPSILON)
                        {
                            if (batchIndex == LAST_ENEMY_ID) // all enemies in batch have arrived
                            {
                                GameSystems.Instance.EnemyBatchComplete(batchId);
                            }

                            Destroy(this.gameObject);
                        }
                    }
                }
                break;
            case Enemy.Patterns.PATTERN_SQUARE:
                {
                    transform.position += (direction.normalized * movementSpeed) * Time.deltaTime;

                    Vector3 diff = new Vector3(0.0f, 0.0f, 0.0f);

                    if (needsMidPointChange)
                        diff = transform.position - midPoint;
                    else
                        diff = transform.position - endPoint;

                    if (!swapDirection)
                    {
                        if (needsMidPointChange)
                        {
                            if (Mathf.Abs(diff.x) <= EPSILON) // arrived at the mid x point
                            {
                                // now move a long the z
                                if (endPoint.z < pos.z)
                                {
                                    direction = new Vector3(0.0f, 0.0f, -1.0f);
                                }
                                else
                                {
                                    direction = new Vector3(0.0f, 0.0f, 1.0f);
                                }

                                moveAxisFirst = AxisMove.AXIS_MOVE_Z;
                                needsMidPointChange = false;
                            }
                        }
                        else
                        {
                            if (moveAxisFirst == AxisMove.AXIS_MOVE_X)
                            {
                                if (Mathf.Abs(diff.x) <= EPSILON)
                                {
                                    swapDirection = true;
                                    if (endPoint.z < pos.z)
                                    {
                                        direction = new Vector3(0.0f, 0.0f, -1.0f);
                                    }
                                    else
                                    {
                                        direction = new Vector3(0.0f, 0.0f, 1.0f);
                                    }
                                }
                            }
                            else
                            if (moveAxisFirst == AxisMove.AXIS_MOVE_Z)
                            {
                                if (Mathf.Abs(diff.z) <= EPSILON)
                                {
                                    swapDirection = true;
                                    if (endPoint.x < pos.x)
                                    {
                                        direction = new Vector3(-1.0f, 0.0f, 0.0f);
                                    }
                                    else
                                    {
                                        direction = new Vector3(1.0f, 0.0f, 0.0f);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if(Mathf.Abs(diff.x) <= EPSILON &&
                            Mathf.Abs(diff.z) <= EPSILON )
                        {
                            if( batchIndex == LAST_ENEMY_ID) // all enemies in batch have arrived
                            {
                                GameSystems.Instance.EnemyBatchComplete(batchId);
                            }
                            Destroy(this.gameObject);
                        }
                    }
                }
                break;
            /*case Enemy.Patterns.PATTERN_CURVE:
                {

                }
                break;
            case Enemy.Patterns.PATTERN_WAVE:
                {

                }
                break;*/
        }
    }

    void ProcessShots()
    {
        if (!isDead)
        {
            timeToShoot += Time.deltaTime;

            if (timeToShoot >= waitShootTime)
            {
                if (canShoot)
                {
                    Vector3 dir = playerShip.transform.position - transform.position;
                    dir.y = 0.0f;

                    GameObject bulletGameObject = (GameObject)Instantiate(Resources.Load("Projectiles/EnemyBullet"), transform.position, transform.rotation);
                    BulletScript bulletScript = bulletGameObject.GetComponent<BulletScript>();

                    bulletScript.moveDirection = dir;
                    bulletScript.projectileSpeed = Random.Range(SHOOT_SPEED_MIN, SHOOT_SPEED_MAX);

                    bulletScript.transform.parent = bulletBatchGroup.transform;

                    timeToShoot = 0.0f;
                    waitShootTime = Random.Range(SHOOT_TIME_MIN, SHOOT_TIME_MAX);

                    AudioSource.PlayClipAtPoint(enemyShoot, Camera.main.transform.position);
                }
            }
        }
    }

    /*void OnCollisionEnter(Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            if (collision.gameObject.tag == "Projectile")
            {
                //print(contact.thisCollider.name + " hit " + contact.otherCollider.name);

                // remove bullet
                Destroy(collision.gameObject);
                Destroy(this.gameObject);
            }
        }
    }*/

    bool IsTargetVisible(Camera c, GameObject go)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(c);
        Vector3 point = go.transform.position;
        foreach (Plane plane in planes)
        {
            if (plane.GetDistanceToPoint(point) < 0)
                return false;
        }
        return true;
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Projectile")
        {
            // hit by player projectile
            if (!isDead)
            {
                GameSystems.Instance.AddToPlayerScore(SCORE_WORTH);
                GameSystems.Instance.AddEnemyToDestroyed(batchId, batchIndex);

                // remove bullet
                Destroy(collider.gameObject);

                // disable draw
                if (renderObjs != null)
                {
                    for (int i = 0; i < renderObjs.Length; ++i)
                        renderObjs[i].enabled = false;
                }
                isDead = true;

                ParticleSystem particleSystem = (ParticleSystem)Instantiate(particlesDeath, transform.position, transform.rotation);
                Destroy(particleSystem.gameObject, particleSystem.duration);

                AudioSource.PlayClipAtPoint(enemyDeath, Camera.main.transform.position);
            }
        }
    }
}
