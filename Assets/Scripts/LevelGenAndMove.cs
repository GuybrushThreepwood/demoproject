﻿using UnityEngine;
using System.Collections;

public class LevelGenAndMove : MonoBehaviour {

    GameObject firstLevelChunk;
    GameObject nextLevelChunk;

    GameObject[] cycleArray = new GameObject[2];

    bool pauseGeneration = false;

    private string[] buildingModelNames = new string[] {
        "Building/building_large", "Building/building_small", "Building/building_normal"
    };

    private float[] rotations = new float[] {
        0.0f,
        90.0f,
        180.0f,
        270.0f
    };

    private string[] level = {

        "#########" +
        "-----C###" +
        "#####^###" +
        "#####^###" +
        "-E-E-D###" +
        "#^#^#####" +
        "#^#^#####",

        "-G-W-----" +
        "###^#####" +
        "---W-E---" +
        "###^#^###" +
        "###H-D###" +
        "-C#^#####" +
        "#^#^#####",

        "-G-W-----" +
        "###^#####" +
        "###^#####" +
        "###^#####" +
        "###^#####" +
        "--C^#####" +
        "##^^#####",

        "##^^#####" +
        "##^^#####" +
        "--GG-C###" +
        "#####^###" +
        "-C###H---" +
        "#^###^###" +
        "#^###^###",

        "#^###^###" +
        "-W---F###" +
        "#^###^###" +
        "#^###^###" +
        "#^###^###" +
        "-G---G---" +
        "#########"
    };

    const float CELL_SIZE = 2.0f;
    const float HALF_CELL_SIZE = CELL_SIZE*0.5f;
    const int TOTAL_CELLS_X = 9;
    const int TOTAL_CELLS_Z = 7;

    const float TOTAL_AXIS_SIZE_X = (CELL_SIZE * (float)TOTAL_CELLS_X);
    const float HALF_AXIS_SIZE_X = (TOTAL_AXIS_SIZE_X * 0.5f);

    const float TOTAL_AXIS_SIZE_Z = (CELL_SIZE * (float)TOTAL_CELLS_Z);
    const float HALF_AXIS_SIZE_Z = (TOTAL_AXIS_SIZE_Z*0.5f);

    const float CELL_START_X = -(HALF_AXIS_SIZE_X - HALF_CELL_SIZE);
    const float CELL_START_Z = HALF_AXIS_SIZE_Z;

    const float LEVEL_MOVE_SPEED = 2.0f;

    int currentLevelChunkIndex = 0;

    bool prepareForWater = false;
    bool doWaterEnd = false;
    bool levelWasReset = true;

    // Use this for initialization
    void Start () {

        currentLevelChunkIndex = level.GetLength(0)-1;

        firstLevelChunk = new GameObject("FIRST");
        nextLevelChunk = new GameObject("NEXT");

        Reset();
    }

    // Update is called once per frame
    void Update () {

        if (!pauseGeneration)
        {
            cycleArray[0].transform.position -= new Vector3(0.0f, 0.0f, LEVEL_MOVE_SPEED * Time.deltaTime);
            cycleArray[1].transform.position -= new Vector3(0.0f, 0.0f, LEVEL_MOVE_SPEED * Time.deltaTime);

            if (cycleArray[0].transform.position.z <= -HALF_AXIS_SIZE_Z - CELL_SIZE)
            {
                // get exact difference over so the new chunk is aligned correctly
                float ztweak = Mathf.Abs((HALF_AXIS_SIZE_Z - CELL_SIZE) - Mathf.Abs(cycleArray[0].transform.position.z));

                // swap the array order
                GameObject swap = cycleArray[0];
                cycleArray[0] = cycleArray[1];
                cycleArray[1] = swap;

                // get ready to build a new chunk at the origin
                cycleArray[1].transform.position = new Vector3(0.0f, 0.0f, 0.0f);

                if (prepareForWater)
                {
                    if (doWaterEnd)
                    {
                        GenerateWaterChunk(cycleArray[1], false, true);

                        if (levelWasReset)
                            levelWasReset = false;
                        else
                            GameSystems.Instance.IncreaseLevelPasses();

                        doWaterEnd = false;
                        prepareForWater = false;
                    }
                    else
                    {
                        GenerateWaterChunk(cycleArray[1], true, false);

                        doWaterEnd = true;
                    }
                }
                else
                    GenerateChunk(cycleArray[1]);

                // now move it to where it should be with the tweak if we went over the time a bit
                cycleArray[1].transform.position = new Vector3(0.0f, 0.0f, (TOTAL_AXIS_SIZE_Z + HALF_AXIS_SIZE_Z + CELL_SIZE) - ztweak);
            }
        }
    }

    public void Reset()
    {
        // set the level to the start position using water only
        levelWasReset = true;

        prepareForWater = false;
        doWaterEnd = false;

        Vector3 nextChunkStartPosition = new Vector3(0.0f, 0.0f, TOTAL_AXIS_SIZE_Z);
        currentLevelChunkIndex = level.GetLength(0) - 1;

        firstLevelChunk.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
        nextLevelChunk.transform.position = new Vector3(0.0f, 0.0f, 0.0f);

        GenerateWaterChunk(firstLevelChunk, false, false);
        GenerateWaterChunk(nextLevelChunk, false, true);

        nextLevelChunk.transform.position = nextChunkStartPosition;

        cycleArray[0] = firstLevelChunk;
        cycleArray[1] = nextLevelChunk;
    }

    void GenerateChunk(GameObject parent)
    {
        // first clear the parent children if there are any
        if (parent.transform.childCount > 0)
        {
            for (int i = parent.transform.childCount - 1; i >= 0; --i)
            {
                GameObject child = parent.transform.GetChild(i).gameObject;
                Destroy(child);
            }
        }

        // move to the first cell position
        float currentX = CELL_START_X;
        float currentZ = CELL_START_Z;

        // iterate
        int index = 1;

        for (int zAxis = 0; zAxis < TOTAL_CELLS_Z; ++zAxis)
        {
            for (int xAxis = 0; xAxis < TOTAL_CELLS_X; ++xAxis)
            {
                int randomModelIndex = Random.Range(0, buildingModelNames.Length);
                int randomRotation = Random.Range(0, rotations.Length);

                char value = level[currentLevelChunkIndex][index-1];

                if (value == '#')
                {
                    int grassOrBuilding = Random.Range(0, 10);

                    if (grassOrBuilding > 6) // building
                    {
                        GameObject modelPrefab = (GameObject)Instantiate(Resources.Load(buildingModelNames[randomModelIndex]), new Vector3(currentX, 0, currentZ), Quaternion.Euler(0.0f, rotations[randomRotation], 0.0f));
                        modelPrefab.name = string.Format("cell_{0}", index);
                        modelPrefab.transform.parent = parent.transform;
                    }
                    else
                    {
                        // grass
                        GameObject modelPrefab = (GameObject)Instantiate(Resources.Load("Ground/grass"), new Vector3(currentX, 0, currentZ), Quaternion.Euler(0.0f, rotations[randomRotation], 0.0f));
                        modelPrefab.name = string.Format("cell_{0}", index);
                        modelPrefab.transform.parent = parent.transform;

                        // on grass generate some random trees
                        int randomTreeCount = Random.Range(10, 20);
                        for (int i = 0; i < randomTreeCount; ++i)
                        {
                            float randomTreeRotation = Random.Range(0.0f, 359.0f);
                            float randomTreeScaleXZ = Random.Range(0.5f, 3.5f);
                            float randomTreeScaleY = Random.Range(1.0f, 5.0f);

                            float randomPosX = Random.Range(currentX - HALF_CELL_SIZE, currentX + HALF_CELL_SIZE);
                            float randomPosZ = Random.Range(currentZ - HALF_CELL_SIZE, currentZ + HALF_CELL_SIZE);

                            GameObject treePrefab = (GameObject)Instantiate(Resources.Load("Ground/tree"), new Vector3(randomPosX, 0, randomPosZ), Quaternion.Euler(0.0f, randomTreeRotation, 0.0f));
                            treePrefab.name = string.Format("tree_{0}", index);
                            treePrefab.transform.parent = modelPrefab.transform;
                            treePrefab.transform.localScale = new Vector3(randomTreeScaleXZ, randomTreeScaleY, randomTreeScaleXZ);
                        }
                    }
                }
                else if(value == '^' )
                {
                    // vertical
                    GameObject modelPrefab = (GameObject)Instantiate(Resources.Load("Ground/road_normal"), new Vector3(currentX, 0, currentZ), Quaternion.Euler(0.0f, 90.0f, 0.0f));
                    modelPrefab.name = string.Format("cell_{0}", index);
                    modelPrefab.transform.parent = parent.transform;
                }
                else if (value == '-')
                {
                    // horizontal
                    GameObject modelPrefab = (GameObject)Instantiate(Resources.Load("Ground/road_normal"), new Vector3(currentX, 0, currentZ), Quaternion.Euler(0.0f, 0.0f, 0.0f));
                    modelPrefab.name = string.Format("cell_{0}", index);
                    modelPrefab.transform.parent = parent.transform;
                }
                else
                {
                    if (value >= 'A' && value <= 'D')
                    {
                        int rotationIndex = (value-'A');

                        GameObject modelPrefab = (GameObject)Instantiate(Resources.Load("Ground/road_corner"), new Vector3(currentX, 0, currentZ), Quaternion.Euler(0.0f, rotations[rotationIndex], 0.0f));
                        modelPrefab.name = string.Format("cell_{0}", index);
                        modelPrefab.transform.parent = parent.transform;
                    }
                    else
                    if (value >= 'E' && value <= 'H')
                    {
                        int rotationIndex = (value - 'E');

                        GameObject modelPrefab = (GameObject)Instantiate(Resources.Load("Ground/road_junction3"), new Vector3(currentX, 0, currentZ), Quaternion.Euler(0.0f, rotations[rotationIndex], 0.0f));
                        modelPrefab.name = string.Format("cell_{0}", index);
                        modelPrefab.transform.parent = parent.transform;
                    }
                    else
                    if (value >= 'W')
                    {
                        GameObject modelPrefab = (GameObject)Instantiate(Resources.Load("Ground/road_junction4"), new Vector3(currentX, 0, currentZ), Quaternion.Euler(0.0f, 0.0f, 0.0f));
                        modelPrefab.name = string.Format("cell_{0}", index);
                        modelPrefab.transform.parent = parent.transform;
                    }
                }
                currentX += CELL_SIZE;
                index++;
           
            }

            currentX = CELL_START_X;
            currentZ -= CELL_SIZE;
        }

        currentLevelChunkIndex--;
        if (currentLevelChunkIndex < 0)
        {
            currentLevelChunkIndex = level.GetLength(0) - 1;
            prepareForWater = true;
        }
    }

    void GenerateWaterChunk(GameObject parent, bool start, bool end )
    {
        // first clear the parent children if there are any
        if (parent.transform.childCount > 0)
        {
            for (int i = parent.transform.childCount - 1; i >= 0; --i)
            {
                GameObject child = parent.transform.GetChild(i).gameObject;
                Destroy(child);
            }
        }

        // move to the first cell position
        float currentX = CELL_START_X;
        float currentZ = CELL_START_Z;

        // iterate
        int index = 1;
        for (int zAxis = 0; zAxis < TOTAL_CELLS_Z; ++zAxis)
        {
            for (int xAxis = 0; xAxis < TOTAL_CELLS_X; ++xAxis)
            {
                int randomRotation = Random.Range(0, rotations.Length);

                if (start == false && 
                    end == false)
                {
                    GameObject modelPrefab = (GameObject)Instantiate(Resources.Load("Ground/water"), new Vector3(currentX, 0, currentZ), Quaternion.Euler(0.0f, rotations[randomRotation], 0.0f));

                    modelPrefab.name = string.Format("cell_{0}", index);
                    modelPrefab.transform.parent = parent.transform;
                }
                else
                {
                    if( start == true )
                    {
                        if (zAxis == TOTAL_CELLS_Z-1)
                        {
                            GameObject modelPrefab = (GameObject)Instantiate(Resources.Load("Ground/water_edge"), new Vector3(currentX, 0, currentZ), Quaternion.Euler(0.0f, 0.0f, 0.0f));

                            modelPrefab.name = string.Format("cell_{0}", index);
                            modelPrefab.transform.parent = parent.transform;
                        }
                        else
                        {
                            GameObject modelPrefab = (GameObject)Instantiate(Resources.Load("Ground/water"), new Vector3(currentX, 0, currentZ), Quaternion.Euler(0.0f, rotations[randomRotation], 0.0f));

                            modelPrefab.name = string.Format("cell_{0}", index);
                            modelPrefab.transform.parent = parent.transform;
                        }
                    }
                    else if( end == true )
                    {
                        if(zAxis == 0 )
                        {
                            GameObject modelPrefab = (GameObject)Instantiate(Resources.Load("Ground/water_edge"), new Vector3(currentX, 0, currentZ), Quaternion.Euler(0.0f, 180.0f, 0.0f));

                            modelPrefab.name = string.Format("cell_{0}", index);
                            modelPrefab.transform.parent = parent.transform;
                        }
                        else
                        {
                            GameObject modelPrefab = (GameObject)Instantiate(Resources.Load("Ground/water"), new Vector3(currentX, 0, currentZ), Quaternion.Euler(0.0f, rotations[randomRotation], 0.0f));

                            modelPrefab.name = string.Format("cell_{0}", index);
                            modelPrefab.transform.parent = parent.transform;
                        }
                    }
                }

                currentX += CELL_SIZE;
                index++;
            }

            currentX = CELL_START_X;
            currentZ -= CELL_SIZE;
        }
    }

    void GenerateRandomChunk(GameObject parent)
    {
        // first clear the parent children if there are any
        if (parent.transform.childCount > 0)
        {
            for (int i = parent.transform.childCount - 1; i >= 0; --i)
            {
                GameObject child = parent.transform.GetChild(i).gameObject;
                Destroy(child);
            }
        }

        // move to the first cell position
        float currentX = CELL_START_X;
        float currentZ = CELL_START_Z;

        // iterate
        int index = 1;
        for (int zAxis = 0; zAxis < TOTAL_CELLS_Z; ++zAxis)
        {
            for (int xAxis = 0; xAxis < TOTAL_CELLS_X; ++xAxis)
            {
                int randomModelIndex = Random.Range(0, buildingModelNames.Length);
                int randomRotation = Random.Range(0, rotations.Length);

                GameObject modelPrefab = (GameObject)Instantiate(Resources.Load(buildingModelNames[randomModelIndex]), new Vector3(currentX, 0, currentZ), Quaternion.Euler(0.0f, rotations[randomRotation], 0.0f));
                modelPrefab.name = string.Format("cell_{0}", index);
                modelPrefab.transform.parent = parent.transform;

                currentX += CELL_SIZE;
                index++;
            }

            currentX = CELL_START_X;
            currentZ -= CELL_SIZE;
        }
    }

    public void SetGenerationState( bool pauseState, bool resetLevel )
    {
        pauseGeneration = pauseState;
        if (resetLevel)
        {
            Reset();
        }
    }
}

