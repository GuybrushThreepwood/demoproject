﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CameraScript : MonoBehaviour {

    Camera cam;
    public bool useRenderTexture;

    const int DEPTH_BPP = 16;

	// Use this for initialization
	void Start () {

        cam = Camera.main;

        if (useRenderTexture)
        {
            if (cam.targetTexture != null)
                cam.targetTexture.Release();

            cam.targetTexture = new RenderTexture(Screen.width, Screen.height, DEPTH_BPP);
        }
	}
	
	// Update is called once per frame
	void Update () {

        if (useRenderTexture)
        {
            if (cam != null)
            {
                // make sure the render texture is the size of the screen
                if (cam.targetTexture.width != Screen.width ||
                    cam.targetTexture.height != Screen.height)
                {
                    if (cam.targetTexture != null)
                        cam.targetTexture.Release();

                    cam.targetTexture = new RenderTexture(Screen.width, Screen.height, DEPTH_BPP);
                }
            }
        }
    }
}
