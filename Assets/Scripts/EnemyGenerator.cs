﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Enemy
{
    public enum Patterns
    {
        PATTERN_STRAIGHT,
        PATTERN_SQUARE,
        //PATTERN_CURVE,
        //PATTERN_WAVE,

        PATTERN_TOTAL
    }
}

public class EnemyGenerator : MonoBehaviour {

    public GameObject enemy;
    /*public*/ int ENEMIES_PER_BATCH = 5;
    /*public*/ float BATCH_SPAWN_RATE = 10.0f;
    /*public*/ float ENEMY_SEPARATION_RATE = 1.0f;

    float currentSpawnTime = 5.0f;

    float lastTimeEnemyCreated = 0.0f;
    int batchCreationCount = 0;
    Enemy.Patterns batchPattern = Enemy.Patterns.PATTERN_STRAIGHT;

    bool pauseGeneration = false;

    bool creatingBatch = false;
    bool spawnedAtTop = false;
    bool needsMidPointChange = false;

    int nextBatchId = 0;

    // points to determine pattern start/end
    List<List<GameObject>> groupedPoints = new List<List<GameObject>>();
    GameObject selectedStartPoint;
    GameObject selectedEndPoint;
    GameObject enemyGroup;
    GameObject bulletBatchGroup;

    // Use this for initialization
    void Start () {
        // patterns
        GameObject leftTopCorner = GameObject.Find("LeftTopCorner");
        GameObject leftBottomCorner = GameObject.Find("LeftBottomCorner");
        GameObject leftTop = GameObject.Find("LeftTop");

        GameObject rightTopCorner = GameObject.Find("RightTopCorner");
        GameObject rightBottomCorner = GameObject.Find("RightBottomCorner");
        GameObject rightTop = GameObject.Find("RightTop");

        bulletBatchGroup = GameObject.Find("BulletBatchGroup");
        enemyGroup = GameObject.Find("EnemyGroup");

        // left top to right bottom
        List<GameObject> pattern = new List<GameObject>();
        pattern.Add(leftTop);
        pattern.Add(rightBottomCorner);
        groupedPoints.Add(pattern);

        // left top to left top corner
        pattern = new List<GameObject>();
        pattern.Add(leftTop);
        pattern.Add(leftTopCorner);
        groupedPoints.Add(pattern);

        // left top to left bottom corner
        pattern = new List<GameObject>();
        pattern.Add(leftTop);
        pattern.Add(leftBottomCorner);
        groupedPoints.Add(pattern);

        // left top to right top corner
        pattern = new List<GameObject>();
        pattern.Add(leftTop);
        pattern.Add(rightTopCorner);
        groupedPoints.Add(pattern);

        /////////////////////////////////////////////////////////////

        // right top to right bottom
        pattern = new List<GameObject>();
        pattern.Add(rightTop);
        pattern.Add(rightBottomCorner);
        groupedPoints.Add(pattern);

        // right top to left top corner
        pattern = new List<GameObject>();
        pattern.Add(rightTop);
        pattern.Add(leftTopCorner);
        groupedPoints.Add(pattern);

        // right top to left bottom corner
        pattern = new List<GameObject>();
        pattern.Add(rightTop);
        pattern.Add(leftBottomCorner);
        groupedPoints.Add(pattern);

        // right top to right top corner
        pattern = new List<GameObject>();
        pattern.Add(rightTop);
        pattern.Add(rightTopCorner);
        groupedPoints.Add(pattern);

        ///////////////////////////////////////////////////////////

        // left top corner to right bottom corner
        pattern = new List<GameObject>();
        pattern.Add(leftTopCorner);
        pattern.Add(rightBottomCorner);
        groupedPoints.Add(pattern);

        // left bottom corner to right top corner
        pattern = new List<GameObject>();
        pattern.Add(leftBottomCorner);
        pattern.Add(rightTopCorner);
        groupedPoints.Add(pattern);
    }

    // Update is called once per frame
    void Update()
    {
        if (!pauseGeneration)
        {
            if (!creatingBatch)
            {
                // create a new batch
                currentSpawnTime += Time.deltaTime;
                if (currentSpawnTime >= BATCH_SPAWN_RATE)
                {
                    // time for a new batch of enemies
                    nextBatchId++;
                    CreateEnemyBatch();
                    currentSpawnTime = 0.0f;
                }

            }
            else
            {
                // spawn another spaced enemy for the current batch
                lastTimeEnemyCreated += Time.deltaTime;
                if (lastTimeEnemyCreated >= ENEMY_SEPARATION_RATE)
                {
                    SpawnEnemy(batchPattern, spawnedAtTop, needsMidPointChange);

                    batchCreationCount++;
                    if (batchCreationCount >= ENEMIES_PER_BATCH)
                    {
                        batchCreationCount = 0;
                        batchPattern = Enemy.Patterns.PATTERN_STRAIGHT;
                        creatingBatch = false;
                    }
                    lastTimeEnemyCreated = 0.0f;
                }
            }
        }
    }

    void CreateEnemyBatch()
    {
        lastTimeEnemyCreated = 0.0f;
        creatingBatch = true;

        batchPattern = (Enemy.Patterns)Random.Range(0, (int)(Enemy.Patterns.PATTERN_TOTAL));
        //batchPattern = Enemy.Patterns.PATTERN_SQUARE;

        // pick a random pattern and mix up which point is the start
        int patternIndex = Random.Range(0, groupedPoints.Count);
        int startIndex = Random.Range(0, 2);

        if (startIndex == 0)
        {
            selectedStartPoint = groupedPoints[patternIndex][0];
            selectedEndPoint = groupedPoints[patternIndex][1];
        }
        else
        {
            selectedStartPoint = groupedPoints[patternIndex][1];
            selectedEndPoint = groupedPoints[patternIndex][0];
        }

        // special case starting from the top means Z movement is always first
        spawnedAtTop = false;
        if (selectedStartPoint.name == "LeftTop" ||
            selectedStartPoint.name == "RightTop" )
        {
            spawnedAtTop = true;
        }

        // special case corner to corner
        needsMidPointChange = false;
        if ((selectedStartPoint.name == "LeftTopCorner" ||
            selectedStartPoint.name == "RightBottomCorner" ||
            selectedStartPoint.name == "RightTopCorner" ||
            selectedStartPoint.name == "LeftBottomCorner" ) &&

            (selectedEndPoint.name == "LeftTopCorner" ||
            selectedEndPoint.name == "RightBottomCorner" ||
            selectedEndPoint.name == "RightTopCorner" ||
            selectedEndPoint.name == "LeftBottomCorner"))
        {
            needsMidPointChange = true;
        }

        SpawnEnemy(batchPattern, spawnedAtTop, needsMidPointChange);

        batchCreationCount = 1;
    }

    void SpawnEnemy( Enemy.Patterns pattern, bool spawnedAtTop, bool needsMidPointChange)
    {
        GameObject newEnemy = (GameObject)Instantiate(enemy, selectedStartPoint.transform.position, enemy.transform.rotation );
        EnemyScript enemyScript = newEnemy.GetComponent<EnemyScript>();

        enemyScript.batchId = nextBatchId;
        enemyScript.batchIndex = batchCreationCount;
        enemyScript.pattern = pattern;
        enemyScript.spawnedAtTop = spawnedAtTop;
        enemyScript.needsMidPointChange = needsMidPointChange;
        enemyScript.endPoint = selectedEndPoint.transform.position;
        enemyScript.bulletBatchGroup = bulletBatchGroup;

        newEnemy.transform.parent = enemyGroup.transform;
    }

    public void SetGenerationState( bool pauseState, bool resetTimer )
    {
        pauseGeneration = pauseState;
        if( resetTimer )
        {
            currentSpawnTime = 0.0f;

            lastTimeEnemyCreated = 0.0f;
        }
    }

    public void DestroyAllEnemies()
    {
        if (bulletBatchGroup.transform.childCount > 0)
        {
            for (int i = bulletBatchGroup.transform.childCount - 1; i >= 0; --i)
            {
                GameObject child = bulletBatchGroup.transform.GetChild(i).gameObject;
                Destroy(child);
            }
        }

        if (enemyGroup.transform.childCount > 0)
        {
            for (int i = enemyGroup.transform.childCount - 1; i >= 0; --i)
            {
                GameObject child = enemyGroup.transform.GetChild(i).gameObject;
                Destroy(child);
            }
        }
    }
}
