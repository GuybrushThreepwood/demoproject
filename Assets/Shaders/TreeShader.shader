﻿Shader "DemoProject/TreeDoubleSided"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Cutoff("Alpha cutoff", Range(0,1)) = 0.5
		_ColorTweak("Color Tweak", Range(0.5,1.0)) = 1.0
	}
	SubShader
	{
		Tags{ "Queue" = "AlphaTest" "IgnoreProjector" = "True" "RenderType" = "TransparentCutout" }
		LOD 100
		Cull Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed _Cutoff;
			fixed _ColorTweak;

			/*float random(float2 p)
			{
				// We need irrationals for pseudo randomness.
				// Most (all?) known transcendental numbers will (generally) work.
				const float2 r = float2(
					23.1406926327792690,  // e^pi (Gelfond's constant)
					2.6651441426902251); // 2^sqrt(2) (Gelfond–Schneider constant)
				return fract(cos(mod(123456789., 1e-7 + 256. * dot(p, r))));
			}*/

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
			
				fixed a = col.a;
				col *= _ColorTweak;
				col.a = a;

				clip(col.a - _Cutoff);

				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
